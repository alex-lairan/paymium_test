require 'json'

data = JSON.parse(File.open('data.json').read)

User = Struct.new(:id, :btc_balance, :eur_balance)
Order = Struct.new(:id, :user_id, :direction, :btc_amount, :price, :state) do
  def match_with(order)
    direction != order.direction &&
      btc_amount == order.btc_amount &&
      price == order.price
  end

  def full_price
    btc_amount * price
  end
end
MatchingOrder = Struct.new(:buy_order, :sell_order, :price, :bitcoins)

users = data['users'].map do |user|
  [
    user['id'],
    User.new(user['id'], user['btc_balance'], user['eur_balance'])
  ]
end.to_h

queued_orders = data['queued_orders'].map do |qo|
  Order.new(qo['id'], qo['user_id'], qo['direction'],
            qo['btc_amount'], qo['price'], 'queued')
end

match_orders = []

# Too much complexity here !
queued_orders.each do |qo1|
  queued_orders.each do |qo2|
    next unless qo1.match_with(qo2)
    seller, buyer = qo1.direction == 'sell' ? [qo1, qo2] : [qo2, qo1]

    next if match_orders.any? { |mo| mo.buy_order.id == buyer.id || mo.sell_order.id == seller.id }

    match_orders << MatchingOrder.new(buyer, seller, qo1.full_price, qo1.btc_amount)
  end
end

match_orders.each do |match|
  buyer = users[match.buy_order.user_id]
  seller = users[match.sell_order.user_id]

  seller.eur_balance += match.price
  seller.btc_balance -= match.bitcoins

  buyer.eur_balance -= match.price
  buyer.btc_balance += match.bitcoins

  match.buy_order.state = 'executed'
  match.sell_order.state = 'executed'
end

orders = queued_orders.select { |qo| qo.state == 'executed' }
queued_orders -= orders

out_data = {
  users: users.values.map(&:to_h),
  queued_orders: queued_orders.map(&:to_h),
  orders: orders.map(&:to_h)
}

output_file = File.open('output.json', 'w')
output_file.write(out_data.to_json)
